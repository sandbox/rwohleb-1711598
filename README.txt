
Media: Wistia

Creates a Wistia PHP Stream Wrapper for Resource and implements the various
formatter and file listing hooks in the Media module.

You must enable API access as described here: http://wistia.com/doc/api-enable
