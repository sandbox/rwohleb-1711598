<?php

/**
 *  @file
 *  Create a Wistia Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $wistia = new ResourceWistiaStreamWrapper('wistia://[video-code]');
 */
class MediaWistiaStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://www.wistia.com/medias/';
  
  function __construct() {
    //parent::__construct();
    
    $this->base_url = 'http://' . media_wistia_variable_get('account_subdomain', 'www') . '.wistia.com/medias/';
  }
  
  function getTarget($f) {
    return FALSE;
  }
  
  static function getMimeType($uri, $mapping = NULL) {
    return 'video/wistia';
  }
  
  function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {
      //return $this->base_url . '?' . http_build_query($parameters);
      return $this->base_url . $parameters['v'];
    }
  }
  
  function getOriginalThumbnailPath() {
    //$parts = $this->get_parameters();
    //return 'http://img.wistia.com/vi/'. check_plain($parts['v']) .'/0.jpg';
    
    $parts = $this->get_parameters();
    
    // Get API key.
    $api_key = media_wistia_variable_get('api_key');
    // Data API provides JSON, uses HTTP Basic Authentication.
    $url = url('https://api:' . $api_key . '@' . MEDIA_WISTIA_API_HOST . '/v1/medias/' . check_plain($parts['v']) . '.json');
    
    if ($info = _media_wistia_fetch_api_data($url)) {
      // Try and return the thumbnail URL
      if (isset($info['thumbnail']->url)) {
        return $info['thumbnail']->url;
      }
    }
    
    // TODO: what do we return on error?
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-wistia/' . check_plain($parts['v']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }
}
