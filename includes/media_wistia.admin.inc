<?php

/**
 * Admin configruation form for media browser settings.
 */
function media_wistia_admin_settings($form, &$form_state) {
  $form = array();
  
  $form[media_wistia_variable_name('api_key')] = array(
    '#type' => 'textfield',
    '#title' => t('Wistia API Password'),
    '#description' => t('Enable API access by following !link, then enter the API password provided.', array('!link' => l(t('these instructions'), 'http://wistia.com/doc/api-enable'))),
    '#default_value' => media_wistia_variable_get('api_key'),
  );
  $form[media_wistia_variable_name('account_subdomain')] = array(
    '#type' => 'textfield',
    '#title' => t('Wistia Account Subdomain'),
    '#description' => t('If your videos are accessible via http://foobar.wistia.com, then you would enter the word foobar.'),
    '#default_value' => media_wistia_variable_get('account_subdomain', 'www'),
  );
  
  return system_settings_form($form);
}
