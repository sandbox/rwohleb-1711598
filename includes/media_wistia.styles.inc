<?php

/**
 * @file media_wistia/includes/media_wistia.styles.inc
 * Styles definitions for Media: Wistia.
 */

/**
 * Implementation of Styles module hook_styles_register().
 */
function media_wistia_styles_register() {
  return array(
    'MediaWistiaStyles' => array(
      'field_types' => 'file',
      'name' => t('MediaWistia'),
      'description' => t('Media Wistia styles.'),
      'path' => drupal_get_path('module', 'media_wistia') .'/includes',
      'file' => 'media_wistia.styles.inc',
    ),
  );
}

/**
 *  Implements hook_styles_containers(). (Deprecated in version 2)
 */
function media_wistia_styles_containers() {
  return array(
    'file' => array(
      'containers' => array(
        'media_wistia' => array(
          'label' => t('Wistia Styles'),
          'data' => array(
            'streams' => array(
              'wistia',
            ),
            'mimetypes' => array(
              'video/wistia',
            ),
          ),
          'weight' => 0,
          'filter callback' => 'media_wistia_formatter_filter',
          'themes' => array(
            'field_formatter_styles' => 'media_wistia_field_formatter_styles',
            'styles' => 'media_wistia_styles',
            'preview' => 'media_wistia_preview_style',
          ),
          'description' => t('Wistia Styles will display embedded Wistia videos and thumbnails to your choosing, such as by resizing, setting colors, and autoplay. You can !manage.', array('!manage' => l(t('manage your Wistia styles here'), 'admin/config/media/media-wistia-styles'))),
        ),
      ),
    ),
  );
}

function media_wistia_formatter_filter($variables) {
  if (isset($variables['object'])) {
    $object = isset($variables['object']->file) ? $variables['object']->file : $variables['object'];
    return (file_uri_scheme($object->uri) == 'wistia') && ($object->filemime == 'video/wistia');
  }
}

/**
 * Implementation of the File Styles module's hook_file_styles_filter().
 */
function media_wistia_file_styles_filter($object) {
  $file = isset($object->file) ? $object->file : $object;
  if ((file_uri_scheme($file->uri) == 'wistia') && ($file->filemime == 'video/wistia')) {
    return 'media_wistia';
  }
}

/**
 *  Implements hook_styles_styles().
 */
function media_wistia_styles_styles() {
  $styles = array(
    'file' => array(
      'containers' => array(
        'media_wistia' => array(
          'styles' => array(
            'wistia_thumbnail' => array(
              'name' => 'wistia_thumbnail',
              'effects' => array(
                array('label' => t('Thumbnail'), 'name' => 'thumbnail', 'data' => array('thumbnail' => 1)),
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 100, 'height' => 75)),
              ),
            ),
            'wistia_preview' => array(
              'name' => 'wistia_preview',
              'effects' => array(
                array('label' => t('Autoplay'), 'name' => 'autoplay', 'data' => array('autoplay' => 0)),
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 220, 'height' => 165)),
              ),
            ),
            'wistia_full' => array(
              'name' => 'wistia_full',
              'effects' => array(
                array('label' => t('Autoplay'), 'name' => 'autoplay', 'data' => array('autoplay' => 0)),
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 640, 'height' => 480)),
                array('label' => t('Full screen'), 'name' => 'fullscreen', 'data' => array('fullscreen' => 1)),
              ),
            ),
          ),
        ),
      ),
    ),
  );

  // Allow any image style to be applied to the thumbnail.
  foreach (image_styles() as $style_name => $image_style) {
    $styles['file']['containers']['media_wistia']['styles']['wistia_thumbnail_' . $style_name] = array(
      'name' => 'wistia_thumbnail_' . $style_name,
      'image_style' => $style_name,
      'effects' => array(
        array('label' => t('Thumbnail'), 'name' => 'thumbnail', 'data' => array('thumbnail' => 1)),
      ),
    );
  }

  return $styles;
}

/**
 *  Implements hook_styles_presets().
 */
function media_wistia_styles_presets() {
  $presets = array(
    'file' => array(
      'square_thumbnail' => array(
        'media_wistia' => array(
          'wistia_thumbnail_square_thumbnail',
        ),
      ),
      'thumbnail' => array(
        'media_wistia' => array(
          'wistia_thumbnail',
        ),
      ),
      'small' => array(
        'media_wistia' => array(
          'wistia_preview',
        ),
      ),
      'large' => array(
        'media_wistia' => array(
          'wistia_full',
        ),
      ),
      'original' => array(
        'media_wistia' => array(
          'wistia_full',
        ),
      ),
    ),
  );
  return $presets;
}

/**
 * Implementation of Styles module hook_styles_default_containers().
 */
function media_wistia_styles_default_containers() {
  // We append Wistia to the file containers.
  return array(
    'file' => array(
      'containers' => array(
        'media_wistia' => array(
          'class' => 'MediaWistiaStyles',
          'name' => 'media_wistia',
          'label' => t('Wistia'),
          'preview' => 'media_wistia_preview_style',
        ),
      ),
    ),
  );
}


/**
 * Implementation of Styles module hook_styles_default_presets().
 */
function media_wistia_styles_default_presets() {
  $presets = array(
    'file' => array(
      'containers' => array(
        'media_wistia' => array(
          'default preset' => 'unlinked_thumbnail',
          'styles' => array(
            'original' => array(
              'default preset' => 'video',
            ),
            'thumbnail' => array(
              'default preset' => 'linked_thumbnail',
            ),
            'square_thumbnail' => array(
              'default preset' => 'linked_square_thumbnail',
            ),
            'medium' => array(
              'default preset' => 'linked_medium',
            ),
            'large' => array(
              'default preset' => 'large_video',
            ),
          ),
          'presets' => array(
            'video' => array(
              array(
                'name' => 'video',
                'settings' => array(),
              ),
            ),
            'large_video' => array(
              array(
                'name' => 'resize',
                'settings' => array(
                  'width' => 640,
                  'height' => 390,
                ),
              ),
              array(
                'name' => 'video',
                'settings' => array(),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  // Allow any image style to be applied to the thumbnail.
  foreach (image_styles() as $style_name => $image_style) {
    $presets['file']['containers']['media_wistia']['presets']['linked_' . $style_name] = array(
      array(
        'name' => 'linkToMedia',
        'settings' => array(),
      ),
      array(
        'name' => 'imageStyle',
        'settings' => array(
          'image_style' => $style_name,
        ),
      ),
      array(
        'name' => 'thumbnail',
        'settings' => array(),
      ),
    );
    $presets['file']['containers']['media_wistia']['presets']['unlinked_' . $style_name] = $presets['file']['containers']['media_wistia']['presets']['linked_' . $style_name];
    array_shift($presets['file']['containers']['media_wistia']['presets']['unlinked_' . $style_name]);
    foreach ($image_style['effects'] as $effect) {
      if (in_array($effect['name'], array('image_scale', 'image_scale_and_crop', 'image_resize', 'image_crop'))) {
        $presets['file']['containers']['media_wistia']['presets']['video_' . $style_name] = array(
          array(
            'name' => 'resize',
            'settings' => $effect['data'],
          ),
          array(
            'name' => 'video',
            'settings' => array(),
          ),
        );
      }
    }
  }
  return $presets;
}
