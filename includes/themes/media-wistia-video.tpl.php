<?php

/**
 * @file media_wistia/includes/themes/media-wistia-video.tpl.php
 *
 * Template file for theme('media_wistia_video').
 *
 * Variables available:
 *  $uri - The uri to the Wistia video, such as wistia://v/g3amdkvq1z.
 *  $video_id - The unique identifier of the Wistia video.
 *  $width - The width to render.
 *  $height - The height to render.
 *  $autoplay - If TRUE, then start the player automatically when displaying.
 *  $fullscreen - Whether to allow fullscreen playback.
 *
 * Note that we set the width & height of the outer wrapper manually so that
 * the JS will respect that when resizing later.
 */
?>
<div class="media-wistia-outer-wrapper" id="media-wistia-<?php print $id; ?>" style="width: <?php print $width; ?>px; height: <?php print $height; ?>px;">
  <div class="media-wistia-preview-wrapper" id="<?php print $wrapper_id; ?>">
    <?php print $output; ?>
  </div>
</div>
