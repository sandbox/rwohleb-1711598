<?php

/**
 * @file media_wistia/includes/themes/media_wistia.theme.inc
 *
 * Theme and preprocess functions for Media: Wistia.
 */

/**
 * Preprocess function for theme('media_wistia_video').
 */
function media_wistia_preprocess_media_wistia_video(&$variables) {
  // Build the URL for display.
  $uri = $variables['uri'];
  $wrapper = file_stream_wrapper_get_instance_by_uri($uri);
  
  $parts = $wrapper->get_parameters();
  $variables['video_id'] = check_plain($parts['v']);
  $variables['query'] = array();
  
  $variables['wrapper_id'] = 'media_wistia_' . $variables['video_id'] . '_' . $variables['id'];
  
  // Pass the settings to replace the object tag with an iframe.
  $settings = array(
    'media_wistia' => array(
      $variables['wrapper_id'] => array(
        'id' => $variables['wrapper_id'] .'_player',
        'video_id' => $variables['video_id'],
      ),
    ),
  );
  
  foreach (array('videoWidth', 'videoHeight', 'autoPlay', 'playButton', 'smallPlayButton', 'playbar', 'volumeControl', 'fullscreenButton', 'controlsVisibleOnLoad', 'playerColor', 'version', 'html5') as $option) {
    // Set the option, either from the options array, or from the default value.
    $variables[$option] = isset($variables[$option]) ? $variables[$option] : (isset($variables['options'][$option]) ? $variables['options'][$option] : media_wistia_variable_get($option));
    
    // Copy the options to the Js settings array.
    $settings['media_wistia'][$variables['wrapper_id']][$option] = $variables[$option];
  }
  
  // Alias the sizes.
  $variables['width'] = $variables['videoWidth'];
  $variables['height'] = $variables['videoHeight'];
  
  // These options default to false.
  foreach (array('autoPlay', 'controlsVisibleOnLoad', 'volumeControl') as $variable) {
    if ($variables[$variable]) {
      $variables['query'][$variable] = 'true';
    }
  }
  // These options default to true.
  foreach (array('fullscreenButton', 'playButton', 'smallPlayButton', 'playbar') as $variable) {
    if (!$variables[$variable]) {
      $variables['query'][$variable] = 'false';
    }
  }
  
  // Copy these as is
  foreach (array('videoWidth', 'videoHeight', 'playerColor') as $variable) {
    if ($variables[$variable]) {
      $variables['query'][$variable] = $variables[$variable];
    }
  }
  
  // Ensure that we pass the required query variables to the Iframe settings.
  if (!empty($variables['query'])) {
    $settings['media_wistia'][$variables['wrapper_id']]['options'] = $variables['query'];
  }
  
  drupal_add_js($settings, 'setting');
  drupal_add_js('http://fast.wistia.com/static/concat/E-v1.js', 'external');
  drupal_add_js(drupal_get_path('module', 'media_wistia') . '/js/media_wistia.js');
  drupal_add_css(drupal_get_path('module', 'media_wistia') . '/css/media_wistia.css');
  drupal_add_js(drupal_get_path('module', 'media_wistia') . '/js/flash_detect_min.js');
  
  $variables['url'] = url('http://fast.wistia.com/embed/iframe/' . $variables['video_id'], array('query' => $variables['query'], 'external' => TRUE, 'https' => FALSE));
  
  $variables['output'] = <<<OUTPUT
    <iframe src="{$variables['url']}"
      allowtransparency="true"
      frameborder="0"
      scrolling="no"
      class="wistia-embed"
      width="{$variables['width']}" height="{$variables['height']}"></iframe>
OUTPUT;
}

function theme_media_wistia_field_formatter_styles($variables) {
  $element = $variables['element'];
  $style = $variables['style'];
  $variables['file'] = $element['#item'];
  $variables['uri'] = $variables['file']['uri'];
  $variables['style_name'] = $style['name'];
  return theme('media_wistia_embed', $variables);
}

/**
 * Preview for Styles UI.
 */
function theme_media_wistia_preview_style($variables) {
  $variables['uri'] = media_wistia_variable_get('preview_uri');
  $variables['field_type'] = 'file';
  $variables['object'] = file_uri_to_object($variables['uri']);

  return theme('styles', $variables);
}

/**
 * NOTE: Deprecated with Styles version 2.
 */
function theme_media_wistia_styles($variables) {
  $style = $variables['style'];
  $variables['file'] = $variables['object'];
  $variables['uri'] = $variables['object']->uri;
  $variables['style_name'] = $style['name'];
  return theme('media_wistia_embed', $variables);
}

/**
 * @todo: get this working
 *
 * This code is for embedding videos in WYSIYWG areas, not currently working.
 * NOTE: Deprecated with Styles version 2.
 */
function theme_media_wistia_embed($variables) {
  $preset_name = $variables['preset_name'];
  $preset = styles_containers_available_styles('file', 'media_wistia', $preset_name);
  $output = '';
  if (!empty($preset)) {
    // Build the URL for display.
    $uri = $variables['uri'];
    $wrapper = file_stream_wrapper_get_instance_by_uri($uri);
    $parts = $wrapper->get_parameters();

    $fullscreen_value = $autoplay = 'false';
    $in_browser = $thumbnail = FALSE;

    foreach ($preset['effects'] as $effect) {
      switch ($effect['name']) {
        case 'autoplay':
          $autoplay = $effect['data']['autoplay'] ? 'true' : 'false';
          break;
        case 'resize':
          $width = $effect['data']['width'];
          $height = $effect['data']['height'];
          break;
        case 'fullscreen':
          $fullscreen_value = $effect['data']['fullscreen'] ? 'true' : 'false';
          break;
        case 'thumbnail':
          $thumbnail = $effect['data']['thumbnail'];
      }
    }
    if (isset($variables['object']->override)) {
      $override = $variables['object']->override;
      if (isset($override['width'])) {
        $width = $override['width'];
      }
      if (isset($override['height'])) {
        $height = $override['height'];
      }
      if (isset($override['wysiwyg'])) {
        $thumbnail = TRUE;
      }
      if (isset($override['browser']) && $override['browser']) {
        $in_browser = TRUE;
        $thumbnail = TRUE;
      }
    }
    $width = isset($width) ? $width : media_wistia_variable_get('width');
    $height = isset($height) ? $height : media_wistia_variable_get('height');
    $video_id = check_plain($parts['v']);
    if ($thumbnail) {
      // @todo Clean this up.
      $image_variables = array(
        'path' => $wrapper->getOriginalThumbnailPath(),
        'alt' => $variables['alt'],
        'title' => $variables['title'],
        'getsize' => FALSE,
      );
      if (isset($preset['image_style'])) {
        $image_variables['path'] = $wrapper->getLocalThumbnailPath();
        $image_variables['style_name'] = $preset['image_style'];
        $output = theme('image_style', $image_variables);
      }
      else {
        // We need to add this style attribute here so that it doesn't get lost
        // If you resize a video in a node, save it, edit it, but don't adjust
        // the sizing of the video while editing, the size will revert to the
        // default.  Adding the specific size here retains the original resizing
        $WYSIWYG = isset($variables['object']->override['style']) ? $variables['object']->override['style'] : '';
        $image_variables['attributes'] = array('width' => $width, 'height' => $height, 'style' => $WYSIWYG);
        $output = theme('image', $image_variables);
      }
      if ($in_browser) {
        // Add an overlay that says 'Wistia' to media library browser thumbnails.
        $output .= '<span />';
      }
    }
    else {
      $output = theme('media_wistia_video', array('uri' => $uri, 'width' => $width, 'height' => $height, 'autoplay' => ($autoplay == 'true' ? TRUE : NULL), 'fullscreen' => ($fullscreen_value == 'true' ? TRUE : NULL)));
    }
  }
  return $output;
}
