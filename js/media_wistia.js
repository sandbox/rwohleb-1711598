
/**
 * @file media_wistia/js/media_wistia.js
 */

(function ($) {

Drupal.media_wistia = {};
Drupal.behaviors.media_wistia = {
  attach: function (context, settings) {
    // Check the browser to see if it supports html5 video.
    var video = document.createElement('video');
    var html5 = video.canPlayType ? true : false;
    
    // If it has video, does it support the correct codecs?
    if (html5) {
      html5 = false;
      if (video.canPlayType( 'video/webm; codecs="vp8, vorbis"' ) || video.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"')) {
        html5 = true;
      }
    }
    
    // Put a prompt in the video wrappers to let users know they need flash
    if (!FlashDetect.installed && !html5){
      $('.media-wistia-preview-wrapper').each(Drupal.media_wistia.needFlash);
    }
    
    // Replace all object tags with iframes.
    if (Drupal.settings && Drupal.settings.media_wistia) {
      for (video in Drupal.settings.media_wistia) {
        Drupal.media_wistia.insertEmbed(video);
      }
    }
  }
};

Drupal.media_wistia.needFlash = function () {
  var id = $(this).attr('id');
  var wrapper = $('.media-wistia-preview-wrapper');
  var hw = Drupal.settings.media_wistia[id].videoHeight / Drupal.settings.media_wistia[id].videoWidth;
  wrapper.html('<div class="js-fallback">' + Drupal.t('You need Flash to watch this video. <a href="@flash">Get Flash</a>', {'@flash':'http://get.adobe.com/flashplayer'}) + '</div>');
  wrapper.height(wrapper.width() * hw);
};

Drupal.media_wistia.insertEmbed = function (embed_id) {
  var videoWrapper = $('#' + embed_id + '.media-wistia-preview-wrapper');
  var settings = Drupal.settings.media_wistia[embed_id];
  
  // Calculate the ratio of the dimensions of the embed.
  settings.hw = settings.videoHeight / settings.videoWidth;
  
  // Allow other modules to modify the video settings.
  $(window).trigger('media_wistia_load', settings);
  
  // Set up the container and add it to the page.
  var video = $('<div class="wistia-player"></div>');
  video.attr('id', settings.id);
  video.attr('data-video-width', settings.videoWidth);
  video.attr('data-video-height', settings.videoHeight);
  video.attr('style', 'width:' + settings.videoWidth + 'px;height:' + settings.videoHeight + 'px;');
  videoWrapper.html(video);
  
  var platform = 'flash';
  if (settings.html5) {
    platform = 'html5';
  }
  
  // Call the Wistia embed function on the container
  wistiaEmbed = Wistia.embed(settings.video_id, {
    version: settings.version,
    videoWidth: settings.videoWidth,
    videoHeight: settings.videoHeight,
    playButton: settings.playButton,
    smallPlayButton: settings.smallPlayButton,
    volumeControl: settings.volumeControl,
    playbar: settings.playbar,
    fullscreenButton: settings.fullscreenButton,
    controlsVisibleOnLoad: settings.controlsVisibleOnLoad,
    playerColor: settings.playerColor,
    autoPlay: settings.autoPlay,
    container: settings.id,
    platformPreference: platform
  });
  
  // Bind a resize event to handle fluid layouts.
  $(window).bind('resize', Drupal.media_wistia.resizeEmbeds);
  
  // For some reason Chrome does not properly size the container around the
  // embed and it will just render the embed at full size unless we set this
  // timeout.
  if (!$('.lightbox-stack').length) {
    setTimeout(Drupal.media_wistia.resizeEmbeds, 1);
  }
};

Drupal.media_wistia.resizeEmbeds = function () {
  $('.media-wistia-preview-wrapper').each(Drupal.media_wistia.resizeEmbed);
};

Drupal.media_wistia.resizeEmbed = function () {
  var context = $(this).parent();
  var video = $(this).children('.wistia-player');
  var hw = Drupal.settings.media_wistia[$(this).attr('id')].hw;
  // Change the height of the wrapper that was given a fixed height by the
  // Wistia theming function.
  $(this)
    .height(context.width() * hw)
    .width(context.width());

  // Change the attributes on the embed to match the new size.
  video
    .height(context.width() * hw)
    .width(context.width());
};

})(jQuery);